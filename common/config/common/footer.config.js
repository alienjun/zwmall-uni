export default {
	services: [{
			icon: 'i-tool',
			name: '预约维修服务',
			linkUrl: 'https://www.mi.com'
		},
		{
			icon: 'i-seven',
			name: '7天无理由退货',
			linkUrl: 'https://www.mi.com'
		},
		{
			icon: 'i-fifteen',
			name: '15天免费换货',
			linkUrl: 'https://www.mi.com'
		},
		{
			icon: 'i-gift',
			name: '满99元包邮',
			linkUrl: 'https://www.mi.com'
		},
		{
			icon: 'i-location',
			name: '520余家售后网点',
			linkUrl: 'https://www.mi.com'
		}
	],
	links: [{
			title: '帮助中心',
			list: [{
					name: '账户管理',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '购物指南',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '订单操作',
					linkUrl: 'https://www.mi.com'
				}
			]
		},
		{
			title: '服务支持',
			list: [{
					name: '售后政策',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '自助服务',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '相关下载',
					linkUrl: 'https://www.mi.com'
				}
			]
		},
		{
			title: '线下门店',
			list: [{
					name: '小米之家',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '服务网点',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '授权体验店',
					linkUrl: 'https://www.mi.com'
				}
			]
		},
		{
			title: '关于小米',
			list: [{
					name: '了解小米',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '加入小米',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '投资者关系',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '廉洁举报',
					linkUrl: 'https://www.mi.com'
				}
			]
		},
		{
			title: '关注我们',
			list: [{
					name: '新浪微博',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '官方微信',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '联系我们',
					linkUrl: 'https://www.mi.com'
				}
			]
		},
		{
			title: '特色服务',
			list: [{
					name: 'F 码通道',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '礼物码',
					linkUrl: 'https://www.mi.com'
				},
				{
					name: '防伪查询',
					linkUrl: 'https://www.mi.com'
				}
			]
		}
	],
	contact: {
		telphoneNumber: '400-100-5678',
		serviceDayRange: '周一至周日',
		serviceTimeRange: '8:00-18:00'
	},
	siteLinks: ['小米商城', 'MIUI', '米家', '米聊', '多看', '游戏', '政企服务', '小米天猫店', '小米集团隐私政策', '小米公司儿童信息保护规则', '小米商城隐私政策',
		'小米商城用户协议', '问题反馈', 'Select Location'
	],
	siteLinkImages: ['https://i1.mifile.cn/f/i/17/site/truste.png', 'https://s01.mifile.cn/i/v-logo-2.png',
		'https://s01.mifile.cn/i/v-logo-1.png', 'https://i8.mifile.cn/b2c-mimall-media/ba10428a4f9495ac310fd0b5e0cf8370.png'
	]
}
